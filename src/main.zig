const std = @import("std");
const mem = std.mem;

pub fn HashMap(comptime K: type, comptime V: type) type {
    return struct {
        const Self = @This();
        const Node = packed struct {
            smaller: ?*Node,
            greater: ?*Node,
            hash: u64,
            value: V,
        };
        const Error = error{
            NotFound,
            IsEmpty,
            InsertError,
            ExistsAlready,
        };
        root: ?*Node,
        allocator: mem.Allocator,

        inline fn alloc(self: *Self) mem.Allocator {
            return self.allocator;
        }

        inline fn isPointer(key: K) bool {
            return switch (@typeInfo(@TypeOf(key))) {
                .Pointer => true,
                else => false,
            };
        }

        inline fn hashkey(key: K) u64 {
            const bytes: []const u8 = std.mem.asBytes(if (isPointer(key)) key else &key);
            return std.hash.CityHash64.hash(bytes);
        }

        pub inline fn init(allocator: mem.Allocator) Self {
            return Self{
                .root = null,
                .allocator = allocator,
            };
        }

        pub fn deinit(self: *Self) void {
            var node = self.root;
            while (node != null) {
                node = freeNode(self.root, self.allocator);
            }
        }

        fn freeNode(node: ?*Node, allocator: mem.Allocator) ?*Node {
            var stack: [128 * @sizeOf(*Node)]u8 = undefined;
            var fba = std.heap.FixedBufferAllocator.init(&stack);
            const fballoc = fba.allocator();
            var node_list = std.ArrayList(*Node).init(fballoc);
            defer node_list.deinit();

            if (node) |nonnil| {
                node_list.append(nonnil) catch @panic("OOM");

                while (true) {
                    const it = node_list.popOrNull();
                    if (it) |curr| {
                        if (curr.smaller != null) node_list.append(curr.smaller.?) catch return it;
                        if (curr.greater != null) node_list.append(curr.greater.?) catch return it;
                        allocator.destroy(curr);
                    } else return null;
                }
            }
            return null;
        }

        pub inline fn isEmpty(self: *Self) bool {
            return (self.root == null);
        }

        fn findParentNode(root: *Node, hash: u64) Error!*Node {
            var it = root;
            while (true) {
                if (hash == it.hash) return Error.ExistsAlready;
                if (hash < it.hash) it = it.smaller orelse return it;
                if (hash > it.hash) it = it.greater orelse return it;
            }
        }

        pub fn insert(self: *Self, key: K, value: V) Error!void {
            const hash = Self.hashkey(key);

            if (self.root != null) {
                var it = self.root.?;
                while (true) {
                    if (hash < it.hash) {
                        if (it.smaller != null) {
                            it = it.smaller.?;
                        } else {
                            it.smaller = self.alloc().create(Node) catch @panic("oom");
                            it.smaller.?.* = Node{ .hash = hash, .value = value, .smaller = null, .greater = null };
                            return;
                        }
                    } else if (hash > it.hash) {
                        if (it.greater != null) {
                            it = it.greater.?;
                        } else {
                            it.greater = self.alloc().create(Node) catch @panic("oom");
                            it.greater.?.* = Node{ .hash = hash, .value = value, .smaller = null, .greater = null };
                            return;
                        }
                    } else {
                        return Error.ExistsAlready;
                    }
                }
            } else {
                self.root = self.alloc().create(Node) catch @panic("oom");
                self.root.?.* = Node{ .hash = hash, .value = value, .smaller = null, .greater = null };
            }
        }

        pub inline fn get(self: *Self, key: K) Error!V {
            const ptr = try self.getRef(key);
            return ptr.*;
        }
        pub inline fn getRef(self: *Self, key: K) Error!*V {
            if (self.isEmpty()) return Error.IsEmpty;
            const hash = Self.hashkey(key);
            if (self.root) |root| {
                var it = root;
                while (true) {
                    if (hash == it.hash) {
                        return &it.value;
                    } else if (hash < it.hash) {
                        if (it.smaller == null) return Error.NotFound;
                        it = it.smaller.?;
                    } else if (hash > it.hash) {
                        if (it.greater == null) return Error.NotFound;
                        it = it.greater.?;
                    }
                }
                return Error.NotFound;
            } else {
                return Error.IsEmpty;
            }
        }
    };
}

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer if (gpa.detectLeaks()) {
        std.debug.print("leaks", .{});
    };
    const alloc = gpa.allocator();

    var map = HashMap([]const u8, u64).init(alloc);
    defer map.deinit();

    try map.insert("hello", 128);
    try map.insert("world", 420);
    try map.insert("friends", 69);

    const value = try map.getRef("hello");
    std.debug.print("{d}\n", .{value.*});
    value.* = 69420;
    const value2 = try map.get("hello");
    std.debug.print("{d}\n", .{value2});
    const world = try map.get("world");
    std.debug.print("{d}\n", .{world});
    const friends = try map.get("friends");
    std.debug.print("{d}\n", .{friends});
}
